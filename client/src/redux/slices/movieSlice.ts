import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IMovie } from '../../interfaces/IMovie';
import { checkFavouriteMovie } from '../../services/favouriteMovieService';
import { getMovieById, getMovies } from '../../services/movieService';
import { getErrorMsg, IReturnedError } from '../../utils/helperFunc';
import { AppThunk, RootState } from '../store';
import { IMovieFilterResponse, MovieSortValues } from '../types';
import { notify } from './notificationSlice';

interface InitialState {
    movies: IMovie[];
    sortBy: MovieSortValues;
    loading: boolean;
    movie: IMovie | null;
    moviePageLoading: boolean;
    isFavouriteMovie: boolean;
    pagesCount: number;
    name: string;
    currentPage: number;
}

const initialState: InitialState = {
    movies: [],
    sortBy: 'rate',
    loading: false,
    movie: null,
    moviePageLoading: false,
    pagesCount: 1,
    isFavouriteMovie: false,
    name: '',
    currentPage: 1,
};

const movieSlice = createSlice({
    name: 'movie',
    initialState,
    reducers: {
        setMovies: (state, action: PayloadAction<{ movies: IMovie[] }>) => {
            state.movies = action.payload.movies;
            state.loading = false;
        },
        sortMovieBy: (state, action: PayloadAction<MovieSortValues>) => {
            state.sortBy = action.payload;
        },
        setIsFavouriteMovie: (state, action: PayloadAction<boolean>) => {
            state.isFavouriteMovie = action.payload;
        },
        setLoading: state => {
            state.loading = true;
        },
        setCurrentMovie: (state, action: PayloadAction<{ movie: IMovie }>) => {
            state.movie = action.payload.movie;
            state.moviePageLoading = false;
        },
        setMoviePageLoading: state => {
            state.moviePageLoading = true;
        },
        setPagesCount: (state, action: PayloadAction<number>) => {
            state.pagesCount = action.payload;
        },
        setName: (state, action: PayloadAction<string>) => {
            state.name = action.payload;
        },
        setCurrentPage: (state, action: PayloadAction<number>) => {
            state.currentPage = action.payload;
        },
    },
});

export const { setMovies, sortMovieBy, setLoading, setCurrentMovie, setMoviePageLoading, setPagesCount, setName, setCurrentPage } =
    movieSlice.actions;

export const fetchMovies = (page: number, sortDirection: MovieSortValues = 'rate', name = ''): AppThunk => {
    return async dispatch => {
        try {
            dispatch(setLoading());
            const moviesData: IMovieFilterResponse = await getMovies(page, sortDirection, name);
            const movies = moviesData.docs;
            const pagesCount = moviesData.pages;
            dispatch(setPagesCount(pagesCount));
            dispatch(setCurrentPage(page));
            dispatch(setMovies({ movies }));
        } catch (e) {
            dispatch(notify(getErrorMsg(e as IReturnedError), 'error'));
        }
    };
};

export const fetchCurrentMovie = (id: string, isAuth: boolean): AppThunk => {
    return async dispatch => {
        try {
            dispatch(setMoviePageLoading());
            const movie = await getMovieById(id);
            if (isAuth) {
                movie.isFavourite = await checkFavouriteMovie(id);
            }
            dispatch(setCurrentMovie({ movie: movie }));
        } catch (e) {
            dispatch(notify(getErrorMsg(e as IReturnedError), 'error'));
        }
    };
};

export const nameChange = (name: string): AppThunk => {
    return async dispatch => {
        try {
            dispatch(setName(name));
        } catch (e) {
            dispatch(notify(getErrorMsg(e as IReturnedError), 'error'));
        }
    };
};

export const selectMovieState = (state: RootState) => state.movie;

export default movieSlice.reducer;
