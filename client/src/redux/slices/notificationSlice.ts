import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppThunk, RootState } from '../store';
import { NotifyPayload } from '../types';

interface InitialState {
    message: string | null;
    type: 'success' | 'error' | 'warning' | null;
}

const initialState: InitialState = {
    message: null,
    type: null,
};

const notificationSilce = createSlice({
    name: 'notify',
    initialState,
    reducers: {
        setNotification: (state, action: PayloadAction<NotifyPayload>) => {
            state.message = action.payload.message;
            state.type = action.payload.type;
        },
        clearNotification: state => {
            state.message = null;
            state.type = null;
        },
    },
});

export const { setNotification, clearNotification } = notificationSilce.actions;

export const notify = (message: string, type: 'success' | 'error' | 'warning'): AppThunk => {
    return dispatch => {
        dispatch(setNotification({ message, type }));
    };
};

export const selectNotifyState = (state: RootState) => state.notify;

export default notificationSilce.reducer;
