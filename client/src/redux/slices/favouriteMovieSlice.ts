import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IMovie } from '../../interfaces/IMovie';
import { addFavouriteMovie, checkFavouriteMovie, deleteFavouriteMovie, getFavouriteMovies } from '../../services/favouriteMovieService';
import { getErrorMsg, IReturnedError } from '../../utils/helperFunc';
import { AppThunk, RootState } from '../store';
import { notify } from './notificationSlice';

interface InitialState {
    favouriteMovies: IMovie[];
    loading: boolean;
}

const initialState: InitialState = {
    favouriteMovies: [],
    loading: false,
};

const favouriteMovieSlice = createSlice({
    name: 'favouriteMovie',
    initialState,
    reducers: {
        setLoading: state => {
            state.loading = true;
        },
        setFavouriteMovies: (state, action: PayloadAction<{ favouriteMovies: IMovie[] }>) => {
            state.favouriteMovies = action.payload.favouriteMovies;
            state.loading = false;
        },
    },
});

export const { setFavouriteMovies, setLoading } = favouriteMovieSlice.actions;

export const fetchFavouriteMovies = (): AppThunk => {
    return async dispatch => {
        try {
            dispatch(setLoading());
            const movies: IMovie[] = await getFavouriteMovies();
            dispatch(setFavouriteMovies({ favouriteMovies: movies }));
        } catch (e) {
            dispatch(notify(getErrorMsg(e as IReturnedError), 'error'));
        }
    };
};

export const addMovieToFavourite = (movieId: string): AppThunk => {
    return async dispatch => {
        try {
            await addFavouriteMovie(movieId);
        } catch (e) {
            dispatch(notify(getErrorMsg(e as IReturnedError), 'error'));
        }
    };
};

export const deleteMovieFromFavourite = (movieId: string): AppThunk => {
    return async dispatch => {
        try {
            await deleteFavouriteMovie(movieId);
        } catch (e) {
            dispatch(notify(getErrorMsg(e as IReturnedError), 'error'));
        }
    };
};

export const checkMovieAlreadyInFavourite = (movieId: string): AppThunk => {
    return async dispatch => {
        try {
            const isFavouriteMovie = await checkFavouriteMovie(movieId);
        } catch (e) {
            dispatch(notify(getErrorMsg(e as IReturnedError), 'error'));
        }
    };
};

export const selectFavouriteMovieState = (state: RootState) => state.favouriteMovie;

export default favouriteMovieSlice.reducer;
