import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IUser } from '../../interfaces/IUser';
import { getUser, logout } from '../../services/authService';
import { getErrorMsg, IReturnedError } from '../../utils/helperFunc';
import storage from '../../utils/localStorage';
import { AppThunk, RootState } from '../store';
import { notify } from './notificationSlice';

interface InitialAuthState {
    isAuth: boolean;
    user: IUser | null;
}

const initialState: InitialAuthState = {
    isAuth: false,
    user: null,
};

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        setUser: (state, action: PayloadAction<{ user: IUser }>) => {
            state.user = action.payload.user;
            state.isAuth = true;
        },
        removeUser: state => {
            state.user = null;
        },
    },
});

export const { setUser, removeUser } = authSlice.actions;

export const fetchUser = (): AppThunk => {
    return async dispatch => {
        try {
            const user: IUser = await getUser();
            dispatch(setUser({ user: user }));
            storage.saveUser(user);
        } catch (e) {
            dispatch(notify(getErrorMsg(e as IReturnedError), 'error'));
        }
    };
};

export const logoutUser = (): AppThunk => {
    return async dispatch => {
        try {
            await logout();
            dispatch(removeUser());
            storage.removeUser();
        } catch (e) {
            dispatch(notify(getErrorMsg(e as IReturnedError), 'error'));
        }
    };
};

export const selectAuthState = (state: RootState) => state.auth;

export default authSlice.reducer;
