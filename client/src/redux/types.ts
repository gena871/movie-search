import { IMovie } from '../interfaces/IMovie';

export interface NotifyPayload {
    message: string;
    type: 'success' | 'error' | 'warning';
}

export type MovieSortValues = 'rate' | 'yearAsc' | 'yearDesc';

export interface IMovieFilterResponse {
    docs: IMovie[];
    pages: number;
}
