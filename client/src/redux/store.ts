import { Action, configureStore } from '@reduxjs/toolkit';
import { ThunkAction } from 'redux-thunk';
import themeReducer from './slices/themeSlice';
import notificationReducer from './slices/notificationSlice';
import movieReducer from './slices/movieSlice';
import authReducer from './slices/authSlice';
import favouriteReducer from './slices/favouriteMovieSlice';

const store = configureStore({
    reducer: {
        theme: themeReducer,
        notify: notificationReducer,
        movie: movieReducer,
        auth: authReducer,
        favouriteMovie: favouriteReducer,
    },
});

export type RootState = ReturnType<typeof store.getState>;

export type AppThunk = ThunkAction<void, RootState, unknown, Action<string>>;

export default store;
