import { Container, Grid, Typography } from '@material-ui/core';
import React from 'react';
import { useNotFoundPageStyles } from '../styles/muiStyles';
import ErrorIcon from '@material-ui/icons/Error';

const NotFoundPage = () => {
    const classes = useNotFoundPageStyles();
    return (
        <div className={classes.root}>
            <Container className={classes.testBorder}>
                <div className={classes.notFoundPagePaper}>
                    <Grid container className={classes.testBorder} style={{ marginTop: '10%' }} justify='center'>
                        <Grid item className={classes.testBorder}>
                            <ErrorIcon style={{ margin: '0 auto', fontSize: 70, color: '#949494' }} />
                        </Grid>
                    </Grid>
                    <Grid container className={classes.testBorder} justify='center'>
                        <Grid item className={classes.testBorder}>
                            <Typography variant='h5'>
                                <b>Страница не найдена</b>
                            </Typography>
                        </Grid>
                    </Grid>
                </div>
            </Container>
        </div>
    );
};

export default NotFoundPage;
