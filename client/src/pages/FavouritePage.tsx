import { Container, Typography } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import MovieCard from '../components/Movie/MovieCard';
import MovieCardSkeleton from '../components/Movie/MovieCardSkeleton';
import { fetchFavouriteMovies, selectFavouriteMovieState } from '../redux/slices/favouriteMovieSlice';
import { useFavouritePageStyles } from '../styles/muiStyles';

const FavouritePage = () => {
    const classes = useFavouritePageStyles();
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchFavouriteMovies());
    }, []);
    const { favouriteMovies, loading } = useSelector(selectFavouriteMovieState);

    return (
        <div className={classes.root}>
            <Container>
                <div className={classes.favouritePagePaper}>
                    <Typography variant='h5'>
                        <b>Избранное</b>
                    </Typography>
                    {loading
                        ? Array(10)
                              .fill(0)
                              .map((item, i) => <MovieCardSkeleton />)
                        : favouriteMovies.map(value => <MovieCard movie={value} />)}
                </div>
            </Container>
        </div>
    );
};

export default FavouritePage;
