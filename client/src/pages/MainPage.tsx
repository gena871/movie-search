import { Container, Grid, InputAdornment, Paper, TextField, Typography } from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import { useForm } from 'react-hook-form';
import React, { ChangeEvent, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import MovieList from '../components/Movie/MovieList';
import SortBar from '../components/SortBar';
import { fetchMovies, nameChange, selectMovieState, sortMovieBy } from '../redux/slices/movieSlice';
import { MovieSortValues } from '../redux/types';
import { useMainPageStyles } from '../styles/muiStyles';
import SearchIcon from '@material-ui/icons/Search';

interface InputValues {
    name: string;
    startYear: string;
    endYear: string;
}

const MainPage = () => {
    const classes = useMainPageStyles();
    const dispatch = useDispatch();
    const [isGenreCollapsed, setIsGenreCollapsed] = useState<boolean>(false);
    const [isYearCollapsed, setIsYearCollapsed] = useState<boolean>(true);
    const { sortBy, currentPage, name } = useSelector(selectMovieState);
    const handleGenreСollapse = () => {
        setIsGenreCollapsed(!isGenreCollapsed);
    };
    const handleYearСollapse = () => {
        setIsYearCollapsed(!isYearCollapsed);
    };

    const handleSortChange = (e: ChangeEvent<{ value: unknown }>) => {
        const selectedValue = e.target.value as MovieSortValues;
        dispatch(sortMovieBy(selectedValue));
        dispatch(fetchMovies(1, selectedValue));
    };

    const handlePaginationChange = (event: ChangeEvent<unknown>, page: number) => {
        dispatch(fetchMovies(page, sortBy));
    };

    const handleNameChange = (event: ChangeEvent<{ value: string }>) => {
        const name: string = event.target.value;
        if (name.length >= 3) {
            dispatch(nameChange(name));
            dispatch(fetchMovies(1, sortBy, name));
        }
        if (name.length == 0) {
            dispatch(fetchMovies(1, sortBy));
        }
    };

    const { register, handleSubmit } = useForm({
        mode: 'onChange',
        defaultValues: {
            name: '',
            startYear: '',
            endYear: '',
        },
    });

    const handleApplyFilters = (values: InputValues) => {
        if (values.name) console.log(values);
    };

    useEffect(() => {
        dispatch(fetchMovies(1));
    }, []);
    const { movies, loading, pagesCount } = useSelector(selectMovieState);

    const menuItems = [
        { value: 'rate', label: 'По рейтингу' },
        { value: 'yearAsc', label: 'По году (на возрастание)' },
        { value: 'yearDesc', label: 'По году (на убывание)' },
    ];

    return (
        <div className={classes.root}>
            <form onSubmit={handleSubmit(handleApplyFilters)}>
                <Container>
                    <div className={classes.mainPagePaper}>
                        <Typography variant='h5'>
                            <b>Фильмы</b>
                        </Typography>
                        <Grid container style={{ marginTop: '10px' }} justifyContent='center' className={classes.testBorder} spacing={2}>
                            {/*<Grid item xs={12} sm={12} md={3} className={classes.testBorder}>*/}
                            {/*    /!*<Paper style={{ padding: '15px' }}>*!/*/}
                            {/*    /!*    <Typography className={classes.filterItemTypography} onClick={handleGenreСollapse}>*!/*/}
                            {/*    /!*        <b>Жанр</b>*!/*/}
                            {/*    /!*    </Typography>*!/*/}
                            {/*    /!*    <Collapse in={isGenreCollapsed}>*!/*/}
                            {/*    /!*        <FormGroup>*!/*/}
                            {/*    /!*            <FormControlLabel control={<Checkbox defaultChecked />} label='Label' />*!/*/}
                            {/*    /!*            <FormControlLabel control={<Checkbox defaultChecked />} label='Label' />*!/*/}
                            {/*    /!*            <FormControlLabel control={<Checkbox defaultChecked />} label='Label' />*!/*/}
                            {/*    /!*        </FormGroup>*!/*/}
                            {/*    /!*    </Collapse>*!/*/}
                            {/*    /!*</Paper>*!/*/}
                            {/*    /!*<Paper style={{ padding: '15px' }}>*!/*/}
                            {/*    /!*    <Typography className={classes.filterItemTypography} onClick={handleYearСollapse}>*!/*/}
                            {/*    /!*        <b>Год</b>*!/*/}
                            {/*    /!*    </Typography>*!/*/}
                            {/*    /!*    <Collapse in={isYearCollapsed}>*!/*/}
                            {/*    /!*        <Grid container className={classes.testBorder} spacing={2}>*!/*/}
                            {/*    /!*            <Grid item md={6} sm={6} xs={6} className={classes.testBorder}>*!/*/}
                            {/*    /!*                <TextField {...register('startYear', { required: false })}*!/*/}
                            {/*    /!*                           name='startYear' helperText="С" fullWidth  />*!/*/}
                            {/*    /!*            </Grid>*!/*/}
                            {/*    /!*            <Grid item md={6} sm={6} xs={6} className={classes.testBorder}>*!/*/}
                            {/*    /!*                <TextField {...register('endYear', { required: false })}*!/*/}
                            {/*    /!*                           name='endYear' helperText="По" fullWidth  />*!/*/}
                            {/*    /!*            </Grid>*!/*/}
                            {/*    /!*        </Grid>*!/*/}
                            {/*    /!*    </Collapse>*!/*/}
                            {/*    /!*</Paper>*!/*/}
                            {/*    /!*<Grid container className={classes.testBorder} justifyContent='flex-end'>*!/*/}
                            {/*    /!*    <Grid item>*!/*/}
                            {/*    /!*        <Button variant='outlined' style={{ marginTop: '10px' }} type='submit'>*!/*/}
                            {/*    /!*            Применить*!/*/}
                            {/*    /!*        </Button>*!/*/}
                            {/*    /!*    </Grid>*!/*/}
                            {/*    /!*</Grid>*!/*/}
                            {/*</Grid>*/}
                            <Grid item container xs={12} sm={12} md={9} className={classes.testBorder} justifyContent='flex-end'>
                                <Grid item xs={12} md={12} sm={12} className={classes.testBorder}>
                                    <Paper style={{ padding: '15px' }} className={classes.testBorder}>
                                        <Grid container className={classes.testBorder}>
                                            <Grid item xs={12} md={12} sm={12} className={classes.testBorder}>
                                                <TextField
                                                    fullWidth
                                                    onChange={handleNameChange}
                                                    label='Название'
                                                    InputProps={{
                                                        startAdornment: (
                                                            <InputAdornment position='start'>
                                                                <SearchIcon />
                                                            </InputAdornment>
                                                        ),
                                                    }}
                                                />
                                            </Grid>
                                            <Grid item xs={12} md={12} sm={12} className={classes.testBorder} style={{ marginTop: '10px' }}>
                                                <SortBar
                                                    sortBy={sortBy}
                                                    label='фильмов'
                                                    sortItems={menuItems}
                                                    handleSortChange={handleSortChange}
                                                />
                                            </Grid>
                                        </Grid>
                                    </Paper>
                                    <MovieList movies={movies} loading={loading} />
                                    <Grid container md justify='center' style={{ marginTop: 'calc(3% + 10px)', bottom: 0 }}>
                                        <Pagination
                                            count={pagesCount}
                                            page={currentPage}
                                            shape='rounded'
                                            onChange={handlePaginationChange}
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </div>
                </Container>
            </form>
        </div>
    );
};

export default MainPage;
