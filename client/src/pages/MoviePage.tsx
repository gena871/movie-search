import { Backdrop, Button, CircularProgress, Container, Grid, Paper, Typography, useMediaQuery, useTheme } from '@material-ui/core';
import BookmarkBorderIcon from '@material-ui/icons/BookmarkBorder';
import YouTubeIcon from '@material-ui/icons/YouTube';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import SimilarMoviesSliderComponent from '../components/Movie/SimilarMoviesSliderComponent';
import RatingViewerComponent from '../components/RatingViewerComponent';
import TrailerDialog from '../components/TrailerDialog';
import { selectAuthState } from '../redux/slices/authSlice';
import { addMovieToFavourite, deleteMovieFromFavourite } from '../redux/slices/favouriteMovieSlice';
import { fetchCurrentMovie, selectMovieState } from '../redux/slices/movieSlice';
import { useMoviePageStyles } from '../styles/muiStyles';
import NumberFormat from 'react-number-format';
import 'swiper/swiper-bundle.min.css';
import 'swiper/swiper.min.css';
import SwiperCore, { Autoplay, Pagination, Navigation } from 'swiper';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';

interface IParams {
    id: string;
}

SwiperCore.use([Autoplay, Pagination, Navigation]);

const MoviePage = () => {
    const classes = useMoviePageStyles();
    const dispatch = useDispatch();
    const { id } = useParams<IParams>();

    const onLoad = (movieId: string) => {
        dispatch(fetchCurrentMovie(movieId, isAuth));
        window.scrollTo(0, 0);
    };

    useEffect(() => {
        onLoad(id);
    }, []);
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down('xs'));
    const [openTrailerDialog, setTrailerDialog] = useState(false);
    const { movie, moviePageLoading } = useSelector(selectMovieState);
    const { isAuth } = useSelector(selectAuthState);

    const handleCloseTrailerDialog = () => {
        setTrailerDialog(false);
    };

    const getDateFormat = (date: string) => {
        const parseDate = Date.parse(date);
        const newDate = new Date(parseDate);
        return newDate.toLocaleDateString();
    };

    const handleSimilarCardClick = (movieId: string) => {
        onLoad(movieId);
    };

    const handleAddToFavourite = (movieId: number) => {
        dispatch(addMovieToFavourite(movieId.toString()));
        onLoad(movieId.toString());
    };

    const handleDeleteFromFavourite = (movieId: number) => {
        dispatch(deleteMovieFromFavourite(movieId.toString()));
        onLoad(movieId.toString());
    };

    return (
        <div className={classes.root}>
            <Backdrop className={classes.backdrop} open={moviePageLoading}>
                <CircularProgress color='inherit' />
            </Backdrop>
            <Container className={classes.testBorder}>
                <Paper className={classes.moviePagePaper}>
                    <Grid container justify='center' className={classes.testBorder}>
                        <Grid item container md={4} xs={12} sm={4} className={classes.testBorder}>
                            <Grid item style={{ margin: '0 auto' }}>
                                <img height={isMobile ? 350 : 450} src={movie?.poster.previewUrl} />
                                <p>
                                    {movie?.videos?.trailers ? (
                                        <Button
                                            variant='outlined'
                                            href={movie?.videos?.trailers[0]?.url}
                                            target='_blank'
                                            style={{ color: '#860101' }}
                                            startIcon={<YouTubeIcon />}
                                        >
                                            Трейлер
                                        </Button>
                                    ) : null}
                                </p>
                                {isAuth ? (
                                    <p>
                                        {movie?.isFavourite ? (
                                            <Button
                                                variant='contained'
                                                onClick={e => {
                                                    if (movie) {
                                                        handleDeleteFromFavourite(movie.id);
                                                    }
                                                }}
                                                startIcon={<DeleteOutlineIcon />}
                                            >
                                                Удалить из избранного
                                            </Button>
                                        ) : (
                                            <Button
                                                variant='outlined'
                                                onClick={e => {
                                                    if (movie) {
                                                        handleAddToFavourite(movie.id);
                                                    }
                                                }}
                                                startIcon={<BookmarkBorderIcon />}
                                            >
                                                Добавить в избранное
                                            </Button>
                                        )}
                                    </p>
                                ) : null}
                            </Grid>
                        </Grid>
                        <Grid item md={6} xs={12} sm={12} className={classes.testBorder}>
                            <Typography variant='h4' style={{ marginTop: '0.5em' }}>
                                <b>
                                    {movie?.name} ({movie?.year})
                                </b>
                            </Typography>
                            <Typography variant='h4' style={{ marginTop: '0.3em', color: '#8d8d8d', fontSize: '12pt' }}>
                                {movie?.alternativeName}
                            </Typography>
                            <Typography variant='h6' style={{ marginTop: '1em' }}>
                                <b>О фильме</b>
                            </Typography>
                            <Grid container className={classes.testBorder} style={{ marginTop: '0.5em' }}>
                                <Grid item md={2} xs={6} className={classes.testBorder}>
                                    <Typography style={{ color: '#8d8d8d', fontSize: '10pt' }}>Год выпуска</Typography>
                                </Grid>
                                <Grid item md={3} xs={6} className={classes.testBorder}>
                                    <Typography style={{ fontSize: '10pt' }}>{movie?.year}</Typography>
                                </Grid>
                            </Grid>
                            <Grid container className={classes.testBorder} style={{ marginTop: '0.5em' }}>
                                <Grid item md={2} xs={6} className={classes.testBorder}>
                                    <Typography style={{ color: '#8d8d8d', fontSize: '10pt' }}>Страны производства</Typography>
                                </Grid>
                                <Grid item md={3} xs={6} className={classes.testBorder}>
                                    <Typography style={{ fontSize: '10pt' }}>
                                        {movie?.countries.map(country => (
                                            <>{country.name} </>
                                        ))}
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Grid container className={classes.testBorder} style={{ marginTop: '0.5em' }}>
                                <Grid item md={2} xs={6} className={classes.testBorder}>
                                    <Typography style={{ color: '#8d8d8d', fontSize: '10pt' }}>Жанры</Typography>
                                </Grid>
                                <Grid item md={7} xs={6} className={classes.testBorder}>
                                    <Typography style={{ fontSize: '10pt' }}>
                                        {movie?.genres.map(genre => (
                                            <>{genre.name} </>
                                        ))}
                                    </Typography>
                                </Grid>
                            </Grid>
                            {movie?.slogan ? (
                                <Grid container className={classes.testBorder} style={{ marginTop: '0.5em' }}>
                                    <Grid item md={2} xs={6} className={classes.testBorder}>
                                        <Typography style={{ color: '#8d8d8d', fontSize: '10pt' }}>Слоган</Typography>
                                    </Grid>
                                    <Grid item md={7} xs={6} className={classes.testBorder}>
                                        <Typography style={{ fontSize: '10pt' }}>"{movie?.slogan}"</Typography>
                                    </Grid>
                                </Grid>
                            ) : null}
                            {movie?.budget ? (
                                <Grid container className={classes.testBorder} style={{ marginTop: '0.5em' }}>
                                    <Grid item md={2} xs={6} className={classes.testBorder}>
                                        <Typography style={{ color: '#8d8d8d', fontSize: '10pt' }}>Бюджет</Typography>
                                    </Grid>
                                    <Grid item md={3} xs={6} className={classes.testBorder}>
                                        <Typography style={{ fontSize: '10pt' }}>
                                            <NumberFormat
                                                value={movie?.budget?.value}
                                                displayType='text'
                                                thousandSeparator=' '
                                                prefix={movie?.budget?.currency}
                                            />
                                        </Typography>
                                    </Grid>
                                </Grid>
                            ) : null}
                            {movie?.premiere?.russia ? (
                                <Grid container className={classes.testBorder} style={{ marginTop: '0.5em' }}>
                                    <Grid item md={2} xs={6} className={classes.testBorder}>
                                        <Typography style={{ color: '#8d8d8d', fontSize: '10pt' }}>Премьера в России</Typography>
                                    </Grid>
                                    <Grid item md={3} xs={6} className={classes.testBorder}>
                                        <Typography style={{ fontSize: '10pt' }}>{getDateFormat(movie?.premiere?.russia)}</Typography>
                                    </Grid>
                                </Grid>
                            ) : null}
                            {movie?.premiere?.world ? (
                                <Grid container className={classes.testBorder} style={{ marginTop: '0.5em' }}>
                                    <Grid item md={2} xs={6} className={classes.testBorder}>
                                        <Typography style={{ color: '#8d8d8d', fontSize: '10pt' }}>Премьера в мире</Typography>
                                    </Grid>
                                    <Grid item md={3} xs={6} className={classes.testBorder}>
                                        <Typography style={{ fontSize: '10pt' }}>{getDateFormat(movie?.premiere?.world)}</Typography>
                                    </Grid>
                                </Grid>
                            ) : null}
                            <Grid container className={classes.testBorder} style={{ marginTop: '0.5em' }}>
                                <Grid item md={2} xs={6} className={classes.testBorder}>
                                    <Typography style={{ color: '#8d8d8d', fontSize: '10pt' }}>Год выпуска</Typography>
                                </Grid>
                                <Grid item md={3} xs={6} className={classes.testBorder}>
                                    <Typography style={{ fontSize: '10pt' }}>2021</Typography>
                                </Grid>
                            </Grid>
                            {movie?.fees?.usa ? (
                                <Grid container className={classes.testBorder} style={{ marginTop: '0.5em' }}>
                                    <Grid item md={2} xs={6} className={classes.testBorder}>
                                        <Typography style={{ color: '#8d8d8d', fontSize: '10pt' }}>Сборы в США</Typography>
                                    </Grid>
                                    <Grid item md={3} xs={6} className={classes.testBorder}>
                                        <Typography style={{ fontSize: '10pt' }}>
                                            <NumberFormat
                                                value={movie?.fees?.usa?.value}
                                                displayType='text'
                                                thousandSeparator=' '
                                                prefix={movie?.fees?.usa?.currency}
                                            />
                                        </Typography>
                                    </Grid>
                                </Grid>
                            ) : null}
                            {movie?.fees?.world ? (
                                <Grid container className={classes.testBorder} style={{ marginTop: '0.5em' }}>
                                    <Grid item md={2} xs={6} className={classes.testBorder}>
                                        <Typography style={{ color: '#8d8d8d', fontSize: '10pt' }}>Сборы в мире</Typography>
                                    </Grid>
                                    <Grid item md={3} xs={6} className={classes.testBorder}>
                                        <Typography style={{ fontSize: '10pt' }}>
                                            <NumberFormat
                                                value={movie?.fees?.world?.value}
                                                displayType='text'
                                                thousandSeparator=' '
                                                prefix={movie?.fees?.world?.currency}
                                            />
                                        </Typography>
                                    </Grid>
                                </Grid>
                            ) : null}
                            {movie?.ratingMpaa ? (
                                <Grid container className={classes.testBorder} style={{ marginTop: '0.5em' }}>
                                    <Grid item md={2} xs={6} className={classes.testBorder}>
                                        <Typography style={{ color: '#8d8d8d', fontSize: '10pt' }}>Рейтинг MPAA</Typography>
                                    </Grid>
                                    <Grid item md={3} xs={6} className={classes.testBorder}>
                                        <Typography style={{ fontSize: '10pt' }}>{movie?.ratingMpaa}</Typography>
                                    </Grid>
                                </Grid>
                            ) : null}
                            {movie?.movieLength ? (
                                <Grid container className={classes.testBorder} style={{ marginTop: '0.5em' }}>
                                    <Grid item md={2} xs={6} className={classes.testBorder}>
                                        <Typography style={{ color: '#8d8d8d', fontSize: '10pt' }}>Время</Typography>
                                    </Grid>
                                    <Grid item md={3} xs={6} className={classes.testBorder}>
                                        <Typography style={{ fontSize: '10pt' }}>
                                            {movie?.movieLength ? <>{movie?.movieLength} мин</> : null}
                                        </Typography>
                                    </Grid>
                                </Grid>
                            ) : null}
                            <Typography variant='h6' style={{ marginTop: '0.5em' }}>
                                <Typography variant='h6' style={{ marginTop: '1em' }}>
                                    <b>Обзор</b>
                                </Typography>
                                <Typography style={{ marginTop: '1em', fontSize: '11pt' }}>{movie?.description}</Typography>
                            </Typography>
                            <Typography variant='h6' style={{ marginTop: '0.5em' }}>
                                <RatingViewerComponent kp={movie?.rating.kp} imdb={movie?.rating.imdb} tmdb={movie?.rating.tmdb} />
                            </Typography>
                        </Grid>
                    </Grid>
                    {movie?.similarMovies ? (
                        <p style={{ marginTop: '2em' }}>
                            <Grid container style={{ marginTop: '2em' }} className={classes.testBorder} justify='center'>
                                <Typography variant='h5'>
                                    <b>Похожие фильмы</b>
                                </Typography>
                                <Grid item md={10} container className={classes.testBorder} style={{ marginTop: '2em' }}>
                                    <SimilarMoviesSliderComponent
                                        similarMovies={movie?.similarMovies}
                                        handleSimilarCardClick={handleSimilarCardClick}
                                    />
                                </Grid>
                            </Grid>
                        </p>
                    ) : null}
                </Paper>
                <TrailerDialog open={openTrailerDialog} handleClose={handleCloseTrailerDialog} />
            </Container>
        </div>
    );
};

const images = [
    'https://www.kinofilms.ua/images/photos/hd/965259.jpg',
    'https://www.kinofilms.ua/images/photos/hd/965259.jpg',
    'https://www.kinofilms.ua/images/photos/hd/965259.jpg',
    'https://www.kinofilms.ua/images/photos/hd/965259.jpg',
    'https://www.kinofilms.ua/images/photos/hd/965259.jpg',
    'https://www.kinofilms.ua/images/photos/hd/965259.jpg',
    'https://www.kinofilms.ua/images/photos/hd/965259.jpg',
    'https://www.kinofilms.ua/images/photos/hd/965259.jpg',
    'https://www.kinofilms.ua/images/photos/hd/965259.jpg',
    'https://www.kinofilms.ua/images/photos/hd/965259.jpg',
];

const responsive = {
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 3,
        paritialVisibilityGutter: 60,
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2,
        paritialVisibilityGutter: 50,
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1,
        paritialVisibilityGutter: 30,
    },
};

export default MoviePage;
