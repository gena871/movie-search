import axios from 'axios';
import { baseApiUrl } from '../Urls';

export const getUser = async () => {
    const response = await axios.get(`${baseApiUrl}/login`, { withCredentials: true });
    return response.data;
};

export const logout = async () => {
    window.open(`${baseApiUrl}/logout`, '_self');
};
