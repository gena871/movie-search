import axios from 'axios';
import { baseApiUrl } from '../Urls';

export const getFavouriteMovies = async () => {
    const response = await axios.get(`${baseApiUrl}/favorite/get`, { withCredentials: true });
    console.log(response.data);
    return response.data;
};

export const addFavouriteMovie = async (movieId: string) => {
    await axios.post(`${baseApiUrl}/favorite/add`, { movieId: movieId }, { withCredentials: true });
    console.log(movieId);
};

export const deleteFavouriteMovie = async (movieId: string) => {
    await axios.delete(`${baseApiUrl}/favorite/delete`, {
        withCredentials: true,
        params: {
            movieId: movieId,
        },
    });
};

export const checkFavouriteMovie = async (movieId: string) => {
    const response = await axios.get(`${baseApiUrl}/favorite/check`, {
        withCredentials: true,
        params: {
            id: movieId,
        },
    });
    return response.data;
};
