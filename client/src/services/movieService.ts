import axios from 'axios';
import { IMovie } from '../interfaces/IMovie';
import { MovieSortValues } from '../redux/types';
import { baseApiUrl } from '../Urls';

export const getMovies = async (page: number, sortType: MovieSortValues, name: string) => {
    const response = await axios.get(`${baseApiUrl}/filterMovies`, {
        params: {
            page: page,
            sortType: sortType,
            name: name,
        },
    });

    return response.data;
};

export const getMovieById = async (id: string) => {
    const response = await axios.get(`${baseApiUrl}/getById`, {
        params: {
            id: id,
        },
    });
    const movie: IMovie = response.data;
    return movie;
};
