import { useState } from 'react';
import { IconButton, Menu, MenuItem } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { useDispatch, useSelector } from 'react-redux';
import { Link as RouterLink } from 'react-router-dom';
import { logoutUser, selectAuthState } from '../redux/slices/authSlice';
import { favouriteUrl } from '../Urls';
import GoogleAuthButton from './GoogleAuthButton';
import ThemeChanger from './ThemeChanger';

interface IMobileMenu {
    isAuth: boolean;
}

const MenuMobileButtons = (props: IMobileMenu) => {
    const { isAuth } = props;
    const dispatch = useDispatch();
    const [openMenu, setOpenMenu] = useState<null | HTMLElement>(null);
    const handleOpenMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
        setOpenMenu(event.currentTarget);
    };

    const handleCloseMenu = () => {
        setOpenMenu(null);
    };

    const handleLogout = () => {
        dispatch(logoutUser());
    };

    const { user } = useSelector(selectAuthState);
    return (
        <div>
            <ThemeChanger />
            <IconButton onClick={handleOpenMenu} style={{ color: 'white' }}>
                <MoreVertIcon />
            </IconButton>
            <Menu
                anchorEl={openMenu}
                getContentAnchorEl={null}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={Boolean(openMenu)}
                onClose={handleCloseMenu}
                marginThreshold={0}
                elevation={1}
            >
                {isAuth ? (
                    <>
                        <MenuItem>{user?.login}</MenuItem>
                        <MenuItem to={favouriteUrl} component={RouterLink}>
                            Избранное
                        </MenuItem>
                        <MenuItem onClick={handleLogout}>Выйти</MenuItem>
                    </>
                ) : (
                    <>
                        <MenuItem>
                            <GoogleAuthButton isMobile={true} />
                        </MenuItem>
                    </>
                )}
            </Menu>
        </div>
    );
};

export default MenuMobileButtons;
