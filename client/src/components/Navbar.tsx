import { AppBar, Button, Container, Toolbar, Typography, useMediaQuery, useTheme } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { Link as RouterLink } from 'react-router-dom';
import { selectAuthState } from '../redux/slices/authSlice';
import { useNavStyles } from '../styles/muiStyles';
import MenuMobileButtons from './MenuMobileButtons';
import MenuDesktopButtons from './MenuDesktopButtons';

const Navbar = () => {
    const classes = useNavStyles();
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down('sm'));
    const { isAuth } = useSelector(selectAuthState);
    // const userIsAuth = isAuth || storage.getUser()
    const userIsAuth = isAuth;
    return (
        <div className={classes.root}>
            <AppBar elevation={1} position='static' className={classes.appBar}>
                <Container>
                    <Toolbar variant='dense' style={{ minHeight: 40 }}>
                        <Typography component='p' style={{ flexGrow: 1 }}>
                            <Button className={classes.menuButton} to='/' component={RouterLink}>
                                Movie-Search
                            </Button>
                        </Typography>
                        {isMobile ? <MenuMobileButtons isAuth={userIsAuth} /> : <MenuDesktopButtons isAuth={userIsAuth} />}
                    </Toolbar>
                </Container>
            </AppBar>
        </div>
    );
};

export default Navbar;
