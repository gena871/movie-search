import { Typography } from '@material-ui/core';
import React from 'react';
import { getTextColorByRating } from '../utils/helperFunc';

interface IProps {
    imdb?: number;
    kp?: number;
    tmdb?: number;
}

const RatingViewerComponent = (props: IProps) => {
    const { imdb, kp, tmdb } = props;

    return (
        <>
            <Typography>
                <b>Рейтинги</b>
            </Typography>
            <Typography>
                {kp ? (
                    <>
                        <span style={{ color: '#8d8d8d', fontSize: '11pt' }}>Кинопоиск: </span>
                        <span
                            style={{
                                color: getTextColorByRating(kp),
                                fontSize: '14pt',
                                fontWeight: 'bold',
                            }}
                        >
                            {kp}
                        </span>
                    </>
                ) : null}
            </Typography>
            <Typography>
                {tmdb ? (
                    <>
                        <span style={{ color: '#8d8d8d', fontSize: '11pt' }}>TMDB: </span>
                        <span
                            style={{
                                color: getTextColorByRating(tmdb),
                                fontSize: '14pt',
                                fontWeight: 'bold',
                            }}
                        >
                            {tmdb}
                        </span>
                    </>
                ) : null}
            </Typography>
            <Typography>
                {imdb ? (
                    <>
                        <span style={{ color: '#8d8d8d', fontSize: '11pt' }}>IMDB: </span>
                        <span
                            style={{
                                color: getTextColorByRating(imdb),
                                fontSize: '14pt',
                                fontWeight: 'bold',
                            }}
                        >
                            {imdb}
                        </span>
                    </>
                ) : null}
            </Typography>
        </>
    );
};

export default RatingViewerComponent;
