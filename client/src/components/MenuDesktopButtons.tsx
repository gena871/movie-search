import { useDispatch, useSelector } from 'react-redux';
import { logoutUser, selectAuthState } from '../redux/slices/authSlice';
import { favouriteUrl } from '../Urls';
import GoogleAuthButton from './GoogleAuthButton';
import ThemeChanger from './ThemeChanger';
import { Button, IconButton, Tooltip } from '@material-ui/core';
import { useNavStyles } from '../styles/muiStyles';
import ExitToAppOutlinedIcon from '@material-ui/icons/ExitToAppOutlined';
import BookmarkBorderOutlinedIcon from '@material-ui/icons/BookmarkBorderOutlined';
import { Link as RouterLink } from 'react-router-dom';

interface IDesktopMenu {
    isAuth: boolean;
}

const MenuDesktopButtons = (props: IDesktopMenu) => {
    const classes = useNavStyles();
    const { isAuth } = props;
    const dispatch = useDispatch();
    const handleLogout = () => {
        dispatch(logoutUser());
    };
    const { user } = useSelector(selectAuthState);

    return (
        <>
            {isAuth ? (
                <>
                    <Button className={classes.menuButton}>{user?.login}</Button>
                    <Tooltip title='Избранное'>
                        <IconButton className={classes.iconButton} to={favouriteUrl} component={RouterLink}>
                            <BookmarkBorderOutlinedIcon />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title='Выйти'>
                        <IconButton className={classes.iconButton} onClick={handleLogout}>
                            <ExitToAppOutlinedIcon />
                        </IconButton>
                    </Tooltip>
                </>
            ) : (
                <GoogleAuthButton isMobile={false} />
            )}
            <ThemeChanger />
        </>
    );
};

export default MenuDesktopButtons;
