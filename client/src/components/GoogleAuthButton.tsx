import { Button } from '@material-ui/core';
import { useNavStyles } from '../styles/muiStyles';
import { baseApiUrl } from '../Urls';
import GoogleIcon from '../icons/GoogleIcon';

interface IProps {
    isMobile: boolean;
}

const GoogleAuthButton = (props: IProps) => {
    const { isMobile } = props;
    const buttonText = 'Войти';
    const classes = useNavStyles();

    return (
        <>
            <form action={`${baseApiUrl}/google`}>
                {isMobile ? (
                    <Button startIcon={<GoogleIcon />} style={{ textTransform: 'inherit' }} type='submit'>
                        {buttonText}
                    </Button>
                ) : (
                    <Button startIcon={<GoogleIcon />} className={classes.menuButton} type='submit'>
                        {buttonText}
                    </Button>
                )}
            </form>
        </>
    );
};

export default GoogleAuthButton;
