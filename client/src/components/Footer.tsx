import { useFooterStyles } from '../styles/muiStyles';
import { Container } from '@material-ui/core';

const Footer = () => {
    const classes = useFooterStyles();

    return (
        <Container>
            <footer className={classes.footer}>
                <p>©movie-search</p>
            </footer>
        </Container>
    );
};

export default Footer;
