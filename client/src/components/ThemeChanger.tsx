import NightsStayOutlinedIcon from '@material-ui/icons/NightsStayOutlined';
import { IconButton, Tooltip } from '@material-ui/core';
import { useNavStyles } from '../styles/muiStyles';
import WbSunnyOutlinedIcon from '@material-ui/icons/WbSunnyOutlined';
import { useDispatch, useSelector } from 'react-redux';
import { selectThemeState, toggleDarkMode } from '../redux/slices/themeSlice';
import storage from '../utils/localStorage';

const ThemeChanger = () => {
    const classes = useNavStyles();
    const dispatch = useDispatch();
    const { darkMode } = useSelector(selectThemeState);
    const handleDarkMode = () => {
        dispatch(toggleDarkMode());
        storage.saveDarkMode(!darkMode);
    };
    return (
        <Tooltip title={darkMode ? 'Светлая тема' : 'Темная тема'}>
            <IconButton onClick={handleDarkMode} className={classes.iconButton}>
                {darkMode ? <WbSunnyOutlinedIcon /> : <NightsStayOutlinedIcon />}
            </IconButton>
        </Tooltip>
    );
};

export default ThemeChanger;
