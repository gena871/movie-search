import { DialogActions, DialogContentText, DialogTitle, useMediaQuery, useTheme } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import React from 'react';

interface IDialogProps {
    open: boolean;
    handleClose: () => void;
}

const TrailerDialog = (props: IDialogProps) => {
    const { open, handleClose } = props;
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down('xs'));

    return (
        <Dialog
            maxWidth={isMobile ? 'xs' : 'md'}
            fullWidth
            open={open}
            onClose={handleClose}
            aria-labelledby='alert-dialog-title'
            aria-describedby='alert-dialog-description'
        >
            <DialogTitle id='alert-dialog-title'>{'Трейлер'}</DialogTitle>
            <DialogContent>
                <DialogContentText style={{ minHeight: '50vh' }}>
                    {isMobile ? (
                        <iframe
                            src='https://widgets.kinopoisk.ru/discovery/trailer/9568?onlyPlayer=1&autoplay=1&cover=1'
                            frameBorder='0'
                            allow='autoplay; encrypted-media'
                            title='video'
                        />
                    ) : (
                        <iframe
                            src='https://widgets.kinopoisk.ru/discovery/trailer/9568?onlyPlayer=1&autoplay=1&cover=1'
                            frameBorder='0'
                            allow='autoplay; encrypted-media'
                            title='video'
                            width='853'
                            height='480'
                        />
                    )}
                </DialogContentText>
                <DialogActions>
                    <Button onClick={handleClose} color='primary'>
                        Закрыть
                    </Button>
                </DialogActions>
            </DialogContent>
        </Dialog>
    );
};

export default TrailerDialog;
