import { Card, CardActionArea, CardContent, CardMedia, Grid, Typography, useMediaQuery, useTheme } from '@material-ui/core';
import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react/swiper-react';
import { ISimilarMovie } from '../../interfaces/IMovie';

interface IProps {
    similarMovies: ISimilarMovie[] | undefined;
    handleSimilarCardClick: (id: string) => void;
}

const SimilarMoviesSliderComponent = (props: IProps) => {
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down('xs'));
    const { similarMovies, handleSimilarCardClick } = props;

    return (
        <Swiper
            slidesPerView={isMobile ? 1 : 4}
            spaceBetween={30}
            slidesPerGroup={isMobile ? 1 : 4}
            loop={true}
            loopFillGroupWithBlank={true}
            autoplay={{
                delay: 10000,
                disableOnInteraction: false,
            }}
            pagination={{
                clickable: true,
            }}
            navigation={true}
        >
            {similarMovies
                ? similarMovies.map(similarMovie => (
                      <SwiperSlide>
                          <Card
                              style={{ boxShadow: 'none' }}
                              onClick={(e: React.MouseEvent<HTMLElement>) => handleSimilarCardClick(similarMovie.id)}
                          >
                              <CardActionArea>
                                  <Grid container justify='center'>
                                      <Grid item style={{ padding: '10px' }}>
                                          <CardMedia>
                                              <img height={200} src={similarMovie.poster.previewUrl} />
                                          </CardMedia>
                                      </Grid>
                                      <Grid item>
                                          <CardContent>
                                              <Typography variant={'h6'} style={{ margin: '0 auto' }}>
                                                  {similarMovie.name}
                                              </Typography>
                                          </CardContent>
                                      </Grid>
                                  </Grid>
                              </CardActionArea>
                          </Card>
                      </SwiperSlide>
                  ))
                : null}
        </Swiper>
    );
};

export default SimilarMoviesSliderComponent;
