import { Card, CardActionArea, CardContent, CardMedia, Grid, Paper, Typography, useMediaQuery, useTheme } from '@material-ui/core';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { IMovie } from '../../interfaces/IMovie';
import { useMainPageStyles } from '../../styles/muiStyles';
import { movieUrl } from '../../Urls';
import RatingViewerComponent from '../RatingViewerComponent';
import TrailerDialog from '../TrailerDialog';

interface IProps {
    movie: IMovie;
}

const MovieCard = (props: IProps) => {
    const classes = useMainPageStyles();
    const theme = useTheme();
    const { movie } = props;
    const isMobile = useMediaQuery(theme.breakpoints.down('xs'));
    const [openTrailerDialog, setTrailerDialog] = useState(false);
    const handleCloseTrailerDialog = () => {
        setTrailerDialog(false);
    };

    const history = useHistory();
    const handleCardClick = (e: React.MouseEvent<HTMLElement>) => {
        e.preventDefault();
        e.stopPropagation();
        history.push(`${movieUrl}/${movie.id}`);
    };

    return (
        <>
            <Paper className={classes.cardPaper}>
                <Card className={classes.card} onClick={handleCardClick}>
                    <CardActionArea className={classes.card}>
                        <Grid container className={classes.testBorder}>
                            <Grid item container md={3} xs={12} sm={3} className={classes.testBorder}>
                                <CardMedia className={classes.cardMedia}>
                                    <img height={200} src={movie.poster.previewUrl} alt='' />
                                </CardMedia>
                            </Grid>
                            <Grid item md={6} xs={12} sm={6} className={classes.testBorder}>
                                <CardContent>
                                    <Typography variant='h6' style={{ marginLeft: '14px' }}>
                                        <b>{movie.name}</b>
                                    </Typography>
                                    <Typography className={classes.smallTypography}>
                                        {movie.alternativeName != null ? (
                                            <>
                                                {movie.alternativeName}, {movie.year}
                                            </>
                                        ) : (
                                            <>{movie.year}</>
                                        )}
                                    </Typography>
                                    <Typography className={classes.smallTypography} style={{ marginTop: '0.5em' }}>
                                        {movie.movieLength ? <>{movie.movieLength} мин</> : null}
                                    </Typography>
                                    {/*<Typography className={classes.smallTypography}>*/}
                                    {/*    {movie.country} • {movie.director}*/}
                                    {/*</Typography>*/}
                                    {/*<Tooltip title='Трейлер'>*/}
                                    {/*    <IconButton*/}
                                    {/*        color='primary'*/}
                                    {/*        style={{ fontSize: '12pt', color: '#860101' }}*/}
                                    {/*        onClick={handleOpenTrailerDialog}*/}
                                    {/*    >*/}
                                    {/*        <YouTubeIcon />*/}
                                    {/*    </IconButton>*/}
                                    {/*</Tooltip>*/}
                                    {/*<Tooltip title='Добавить в избранное'>*/}
                                    {/*    <IconButton color='primary' style={{ fontSize: '12pt' }}>*/}
                                    {/*        <BookmarkBorderIcon />*/}
                                    {/*    </IconButton>*/}
                                    {/*</Tooltip>*/}
                                </CardContent>
                            </Grid>
                            <Grid item md={3} xs={12} sm={3} className={classes.testBorder}>
                                <Typography variant='h6'>
                                    <CardContent>
                                        <RatingViewerComponent kp={movie?.rating.kp} imdb={movie?.rating.imdb} tmdb={movie?.rating.tmdb} />
                                    </CardContent>
                                </Typography>
                            </Grid>
                        </Grid>
                    </CardActionArea>
                </Card>
            </Paper>
            <TrailerDialog open={openTrailerDialog} handleClose={handleCloseTrailerDialog} />
        </>
    );
};

export default MovieCard;
