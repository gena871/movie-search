import { Card, CardActionArea, CardContent, Grid, Paper, Typography, useMediaQuery, useTheme } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import { useMainPageStyles } from '../../styles/muiStyles';

const MovieCardSkeleton = () => {
    const classes = useMainPageStyles();
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down('xs'));

    return (
        <>
            <Paper className={classes.cardPaper}>
                <Card className={classes.card}>
                    <CardActionArea className={classes.card}>
                        <Grid container className={classes.testBorder}>
                            <Grid item container md={3} xs={12} sm={3} className={classes.testBorder}>
                                <Skeleton
                                    className={classes.cardMedia}
                                    animation='wave'
                                    variant='rect'
                                    style={{ height: '100%', width: '100%', maxWidth: '900px', borderRadius: '1%' }}
                                    height={300}
                                />
                            </Grid>
                            <Grid item md={6} xs={12} sm={6} className={classes.testBorder}>
                                <CardContent>
                                    <Typography variant='h6' style={{ marginLeft: '14px' }}>
                                        <b>
                                            <Skeleton animation='wave' width='80%' />
                                        </b>
                                    </Typography>
                                    <Typography className={classes.smallTypography}>
                                        <Skeleton animation='wave' width='80%' />
                                    </Typography>
                                    <Typography className={classes.smallTypography} style={{ marginTop: '0.5em' }}>
                                        <Skeleton animation='wave' width='80%' />
                                    </Typography>
                                    <Typography className={classes.smallTypography}>
                                        <Skeleton animation='wave' width='80%' />
                                    </Typography>
                                </CardContent>
                            </Grid>
                            <Grid item md={3} xs={12} sm={3} className={classes.testBorder}>
                                <Typography variant='h6'>
                                    <CardContent>
                                        <Typography>
                                            <Skeleton animation='wave' width='80%' />
                                        </Typography>
                                        <Typography>
                                            <Skeleton animation='wave' width='80%' />
                                        </Typography>
                                        <Typography>
                                            <Skeleton animation='wave' width='80%' />
                                        </Typography>
                                        <Typography>
                                            <Skeleton animation='wave' width='80%' />
                                        </Typography>
                                    </CardContent>
                                </Typography>
                            </Grid>
                        </Grid>
                    </CardActionArea>
                </Card>
            </Paper>
        </>
    );
};

export default MovieCardSkeleton;
