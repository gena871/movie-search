import React from 'react';
import { IMovie } from '../../interfaces/IMovie';
import MovieCard from './MovieCard';
import MovieCardSkeleton from './MovieCardSkeleton';

interface IProps {
    movies: IMovie[];
    loading: boolean;
}

const MovieList = (props: IProps) => {
    const { movies, loading } = props;

    return (
        <>
            {loading
                ? Array(10)
                      .fill(0)
                      .map((item, i) => <MovieCardSkeleton />)
                : movies.map(value => <MovieCard movie={value} />)}
        </>
    );
};

export default MovieList;
