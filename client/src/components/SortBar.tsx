import { MenuItem, Select, Typography } from '@material-ui/core';
import React from 'react';
interface IProps {
    sortBy: string;
    label: string;
    sortItems: { value: string; label: string }[];
    handleSortChange: (e: React.ChangeEvent<{ value: unknown }>) => void;
}

const SortBar = (props: IProps) => {
    const { label, sortItems, sortBy, handleSortChange } = props;

    return (
        <>
            <Typography display='inline'>Сортировка {label}: </Typography>
            <Select value={sortBy} onChange={handleSortChange}>
                {sortItems.map(value => (
                    <MenuItem value={value.value}>{value.label}</MenuItem>
                ))}
            </Select>
        </>
    );
};

export default SortBar;
