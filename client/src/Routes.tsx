import { Container } from '@material-ui/core';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect, Route, Switch, useHistory } from 'react-router-dom';
import FavouritePage from './pages/FavouritePage';
import MainPage from './pages/MainPage';
import MoviePage from './pages/MoviePage';
import NotFoundPage from './pages/NotFoundPage';
import { fetchUser, selectAuthState } from './redux/slices/authSlice';
import { baseApiUrl, favouriteUrl, movieUrl } from './Urls';

const Routes = () => {
    const { isAuth } = useSelector(selectAuthState);

    return (
        <Container>
            <Switch>
                <Route exact path='/'>
                    <MainPage />
                </Route>
                <Route exact path={favouriteUrl}>
                    {isAuth ? <FavouritePage /> : <Redirect to='/' />}
                </Route>
                <Route exact path='/login/success'>
                    <LoginSuccess />
                </Route>
                <Route exact path={`${movieUrl}/:id`}>
                    <MoviePage />
                </Route>
                <Route>
                    <NotFoundPage />
                </Route>
            </Switch>
        </Container>
    );
};

export default Routes;

const LoginSuccess = () => {
    const history = useHistory();
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchUser());
        history.push('/');
    }, []);
    return <></>;
};
