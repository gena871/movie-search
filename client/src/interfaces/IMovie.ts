export interface IMovie {
    name: string;
    year: number;
    genre: string;
    duration: string;
    country: string;
    director: string;
    imageLink: string;

    id: number;
    poster: {
        previewUrl: string;
        url: string;
    };
    description: string;
    rating: {
        imdb?: number;
        kp?: number;
        tmdb?: number;
    };
    alternativeName: string;
    movieLength: number;
    countries: {
        name: string;
    }[];
    genres: {
        name: string;
    }[];
    slogan: string;
    budget: {
        currency: string;
        value: string;
    };
    premiere: {
        world: string;
        russia: string;
    };
    ratingMpaa: string;
    fees: {
        usa: {
            currency: string;
            value: string;
        };
        world: {
            currency: string;
            value: string;
        };
    };
    productionCompanies: {
        name: string;
    }[];
    similarMovies: ISimilarMovie[];

    videos: {
        trailers: {
            url: string;
        }[];
    };

    isFavourite: boolean;
}

export interface ISimilarMovie {
    id: string;
    name: string;
    poster: {
        previewUrl: string;
        url: string;
    };
}
