import './App.css';
import React, { useEffect } from 'react';
import { ThemeProvider } from '@material-ui/core';
import Footer from './components/Footer';
import { useDispatch, useSelector } from 'react-redux';
import Navbar from './components/Navbar';
import { fetchUser } from './redux/slices/authSlice';
import Routes from './Routes';
import theme from './styles/customTheme';
import { useBodyStyles } from './styles/muiStyles';
import { selectThemeState, toggleDarkMode } from './redux/slices/themeSlice';
import storage from './utils/localStorage';
import NotificationBox from './components/NotificationBox';

const App = () => {
    const { darkMode } = useSelector(selectThemeState);
    const classes = useBodyStyles(darkMode)();
    const dispatch = useDispatch();
    useEffect(() => {
        const loadedDarkMode = storage.loadDarkMode();
        if (loadedDarkMode && !darkMode) {
            dispatch(toggleDarkMode());
        }

        const loadedUser = storage.getUser();
        if (loadedUser) {
            dispatch(fetchUser());
        }
    }, []);
    return (
        <ThemeProvider theme={theme(darkMode)}>
            <div className={classes.root}>
                <Navbar />
                <Routes />
                <NotificationBox />
                <Footer />
            </div>
        </ThemeProvider>
    );
};

export default App;
