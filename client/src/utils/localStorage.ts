import { IUser } from '../interfaces/IUser';

const storageKeyDarkMode = 'MovieSearcherDarkMode';

const saveDarkMode = (value: boolean) => {
    localStorage.setItem(storageKeyDarkMode, String(value));
};

const loadDarkMode = () => {
    const darkMode = localStorage.getItem(storageKeyDarkMode);

    if (darkMode) {
        return JSON.parse(darkMode);
    }

    return null;
};

const saveUser = (user: IUser) => {
    localStorage.setItem('user', JSON.stringify(user));
};

const getUser = () => {
    const user = localStorage.getItem('user');
    if (user) {
        return JSON.parse(user);
    }

    return null;
};

const removeUser = () => localStorage.removeItem('user');

const storage = { saveDarkMode, loadDarkMode, saveUser, getUser, removeUser };

export default storage;
