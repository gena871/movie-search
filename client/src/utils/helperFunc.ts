export interface IReturnedError {
    response?: {
        data?: {
            message: string;
        };
    };
    message: string;
}

export const getErrorMsg = (err: IReturnedError) => {
    if (err?.response?.data?.message) {
        return err.response.data.message;
    } else {
        return err.message;
    }
};

export const getTextColorByRating = (rating: number) => {
    if (rating <= 3) {
        return '#860101';
    }
    if (8 > rating && rating > 3) {
        return '#ee940d';
    }
    return '#3d8601';
};
