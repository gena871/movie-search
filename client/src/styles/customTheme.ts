import { createTheme } from '@material-ui/core/styles';

const theme = (darkMode: boolean) =>
    createTheme({
        palette: {
            type: darkMode ? 'dark' : 'light',
            secondary: {
                main: darkMode ? '#360000' : '#660000',
                dark: darkMode ? '#ffffff' : '#000000',
            },
            primary: {
                main: '#00143e',
                light: '#001F54',
            },
        },
    });

export default theme;
