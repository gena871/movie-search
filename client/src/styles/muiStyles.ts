import { makeStyles } from '@material-ui/core/styles';
import theme from './customTheme';

export const useBodyStyles = (darkMode: boolean) =>
    makeStyles(
        () => ({
            root: {
                backgroundColor: darkMode ? '#333' : '#f9f9f9',
                width: '100vW',
                display: 'flex',
                flexDirection: 'column',
                flex: 1,
                minHeight: '100vH',
            },
        }),
        { index: 1 },
    );

export const useMoviePageStyles = makeStyles(theme => ({
    root: {
        [theme.breakpoints.down('xs')]: {
            padding: '0.5em 0.5em',
        },
    },
    moviePagePaper: {
        color: theme.palette.secondary.dark,
        fontFamily: 'Nunito',
        padding: '1.5em',
        minHeight: 'calc(100vH - 130px)',
        [theme.breakpoints.down('xs')]: {
            padding: '0.5em 0.7em',
            minHeight: 'calc(100vH - 80px)',
        },
    },
    testBorder: {
        //border:'2px solid black'
    },
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
}));

export const useMainPageStyles = makeStyles(theme => ({
    root: {
        [theme.breakpoints.down('xs')]: {
            padding: '0.5em 0.5em',
        },
    },
    mainPagePaper: {
        color: theme.palette.secondary.dark,
        fontFamily: 'Nunito',
        padding: '1.5em',
        minHeight: 'calc(100vH - 130px)',
        [theme.breakpoints.down('xs')]: {
            padding: '0.5em 0.7em',
            minHeight: 'calc(100vH - 80px)',
        },
    },
    testBorder: {
        //border:'2px solid black'
    },
    cardMedia: {
        [theme.breakpoints.down('xs')]: {
            margin: '0 auto',
        },
    },
    cardPaper: {
        padding: '10px',
        marginTop: '10px',
    },
    card: {
        border: 'none',
        boxShadow: 'none',
    },
    smallTypography: {
        marginLeft: '14px',
        color: '#8d8d8d',
        fontSize: '11pt',
    },
    filterItemTypography: {
        cursor: 'pointer',
    },
}));

export const useNavStyles = makeStyles({
    root: {
        position: 'sticky',
        top: 0,
        zIndex: 100,
        width: '100%',
    },
    menuButton: {
        textTransform: 'inherit',
        size: '24px',
        fontSize: '12pt',
        color: '#ffffff',
    },
    appBar: {
        paddingTop: '0.2em',
        paddingBottom: '0.2em',
        color: '#ffffff',
    },
    iconButton: {
        color: '#ffffff',
        textTransform: 'inherit',
        size: '24px',
        fontSize: '12pt',
        padding: '0.35em',
    },
});

export const useFooterStyles = makeStyles(
    theme => ({
        footer: {
            bottom: 0,
            opacity: '50%',
            textAlign: 'center',
            color: theme.palette.secondary.dark,
        },
    }),
    { index: 1 },
);

export const useNotFoundPageStyles = makeStyles(
    theme => ({
        root: {
            position: 'sticky',
            top: 0,
            zIndex: 100,
            width: '100%',
        },
        notFoundPagePaper: {
            color: theme.palette.secondary.dark,
            fontFamily: 'Nunito',
            padding: '1.5em',
            minHeight: 'calc(100vH - 130px)',
            [theme.breakpoints.down('xs')]: {
                padding: '0.5em 0.7em',
                minHeight: 'calc(100vH - 80px)',
            },
        },
        testBorder: {
            //border:'2px solid black'
        },
    }),
    { index: 1 },
);

export const useFavouritePageStyles = makeStyles(
    theme => ({
        root: {
            position: 'sticky',
            top: 0,
            zIndex: 100,
            width: '100%',
        },
        favouritePagePaper: {
            color: theme.palette.secondary.dark,
            fontFamily: 'Nunito',
            padding: '1.5em',
            minHeight: 'calc(100vH - 130px)',
            [theme.breakpoints.down('xs')]: {
                padding: '0.5em 0.7em',
                minHeight: 'calc(100vH - 80px)',
            },
        },
    }),
    { index: 1 },
);
