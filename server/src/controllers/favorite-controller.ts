import axios from 'axios';
import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { FavouriteUser } from '../entity/FavouriteUser';

export default class filterMovieController {
    public async getFavorite(req: Request, res: Response): Promise<Response> {
        //@ts-ignore
        const userId = req.user.id;

        const favouriteMoviesData = await getRepository(FavouriteUser).find({
            where: {
                user_id: userId,
            },
            relations: ['user'],
        });

        const favouriteMovies: any = [];

        for await (let item of favouriteMoviesData) {
            const movie = await axios.get(
                `${process.env.API_BASE_URL}/movie?field=id&search=${item.movieId}&token=${process.env.API_TOKEN}`,
            );
            favouriteMovies.push(movie.data);
        }

        return res.send(favouriteMovies).status(200);
    }

    public async addToFavorite(req: Request, res: Response): Promise<Response> {
        //@ts-ignore
        const userId = req.user.id;
        const { movieId } = req.body;

        if (movieId == null) {
            return res.status(400).send('Не указан movieId');
        }

        const favouriteUser = new FavouriteUser();
        favouriteUser.user_id = userId;
        favouriteUser.movieId = movieId.toString();
        favouriteUser.createdAt = new Date();
        favouriteUser.updatedAt = new Date();

        await getRepository(FavouriteUser).save(favouriteUser);

        return res.status(201).send('added');
    }

    public async deleteToFavorite(req: Request, res: Response): Promise<Response> {
        //@ts-ignore
        const userId = req.user.id;
        const { movieId } = req.query;

        if (movieId == null) {
            return res.status(400).send('Не указан movieId');
        }

        await getRepository(FavouriteUser).delete({
            movieId: movieId.toString(),
            user_id: userId,
        });
        return res.sendStatus(200);
    }

    public async checkMovieIsFavourite(req: Request, res: Response): Promise<Response> {
        //@ts-ignore
        const userId = req.user.id;
        const { id } = req.query;

        if (id == null) {
            return res.status(400).send('Не указан movie_id');
        }

        const [list, count] = await getRepository(FavouriteUser).findAndCount({
            where: [{ user_id: userId, movieId: id }],
        });

        return res.status(200).json(count >= 1);
    }
}
