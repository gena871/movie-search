import { Request, Response } from 'express';

export default class AuthController {
    public async login(req: Request, res: Response): Promise<Response> {
        // @ts-ignore
        return res.json(req.user).status(200);
    }

    public async failed(req: Request, res: Response) {
        const email = req.query.email;
    }

    public async success(req: Request, res: Response): Promise<Response> {
        // @ts-ignore
        console.log(req.user.email);
        // @ts-ignore
        return res.send(`Welcome ${req.user?.email}`);
    }

    public async googleRedirect(req: Request, res: Response) {
        res.redirect(`${process.env.CLIENT_BASE_URL}/login/success`);
    }

    public async logout(req: Request, res: Response) {
        req.logout();
        res.header('Access-Control-Allow-Origin', `${process.env.CLIENT_BASE_URL}`);
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
        res.redirect(`${process.env.CLIENT_BASE_URL}`);
    }
}
