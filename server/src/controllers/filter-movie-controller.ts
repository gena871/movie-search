import { Request, Response } from 'express';
import axios from 'axios';

export default class filterMovieController {
    public async heathCheck(req: Request, res: Response): Promise<Response> {
        return res.sendStatus(200);
    }

    public async getById(req: Request, res: Response): Promise<Response> {
        const { id } = req.query;
        if (!id) {
            return res.status(400).send('Не указан id');
        }

        const query = `${process.env.API_BASE_URL}/movie?field=id&search=${id}&token=${process.env.API_TOKEN}`;
        let response1;
        await axios.get(query).then(response => {
            response1 = response.data;
        });

        return res.status(200).send(response1);
    }

    public async filterMovies(req: Request, res: Response): Promise<Response> {
        let year, genres, name, page;
        req.query.year != null ? (year = req.query.year) : (year = '');
        req.query.genres != null ? (genres = req.query.genres) : (genres = '');
        req.query.name != null ? (name = req.query.name) : (name = '');
        req.query.page != null ? (page = req.query.page) : (page = 1);
        const { sortType } = req.query;

        let sortField = 'year';
        let sortTypeValue = '-1';

        if (sortType == 'yearAsc') {
            sortField = 'year';
            sortTypeValue = '1';
        }
        if (sortType == 'yearDesc') {
            sortField = 'year';
            sortTypeValue = '-1';
        }
        if (sortType == 'rate') {
            sortField = 'rating.kp';
            sortTypeValue = '-1';
        }

        let query: string = `${process.env.API_BASE_URL}/movie?`;
        year != '' && year != null && typeof year == 'string' ? (query += 'field=year&search=' + year + '&') : null;
        genres != '' && genres != null && typeof genres == 'string' ? (query += 'field=genres.name&search=' + genres + '&') : null;
        name != '' && name != null && typeof name == 'string' ? (query += 'field=name&search=' + name + '&') : null;

        query += `page=${page}&field=typeNumber&search=1&sortField=${sortField}&sortType=${sortTypeValue}&token=${process.env.API_TOKEN}`;
        let response1;

        await axios.get(encodeURI(query)).then(response => {
            response1 = response.data;
        });

        return res.status(200).send(response1);
    }
}
