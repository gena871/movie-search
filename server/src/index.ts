import http from 'http';
import app from './app';
import { connectDb } from './db';

connectDb();

let server = http.createServer(app);
const PORT = process.env.PORT as string;

server.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});
