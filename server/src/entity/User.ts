import { Column, Entity, Index, OneToMany } from 'typeorm';
import { FavouriteUser } from './FavouriteUser';

@Index('user_pk', ['id'], { unique: true })
@Entity('user', { schema: 'public' })
export class User {
    @Column('uuid', {
        primary: true,
        name: 'id',
        default: () => 'uuid_generate_v4()',
    })
    id: string;

    @Column('character varying', { name: 'login', length: 40 })
    login: string;

    @Column('character varying', { name: 'email', length: 40 })
    email: string;

    @Column('character varying', { name: 'googleId', length: 40 })
    googleId: string;

    @Column('timestamp without time zone', {
        name: 'created_at',
        default: () => 'now()',
    })
    createdAt: Date;

    @Column('timestamp without time zone', {
        name: 'updated_at',
        default: () => 'now()',
    })
    updatedAt: Date;

    @OneToMany(() => FavouriteUser, favouriteUser => favouriteUser.user)
    favouriteUsers: FavouriteUser[];
}

export type UserDocument = {
    id: string;
    username: string;
    email: string;
    googleId: string;
};
