import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './User';

@Index('user_favourite_pk', ['id'], { unique: true })
@Entity('favourite_user', { schema: 'public' })
export class FavouriteUser {
    @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
    id: number;

    @Column('character varying', { name: 'user_id' })
    user_id: string;

    @Column('character varying', { name: 'movie_id', length: 40 })
    movieId: string;

    @Column('timestamp without time zone', {
        name: 'created_at',
        default: () => 'now()',
    })
    createdAt: Date;

    @Column('timestamp without time zone', {
        name: 'updated_at',
        default: () => 'now()',
    })
    updatedAt: Date;

    @ManyToOne(() => User, users => users.favouriteUsers, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    })
    @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
    user: User;
}
