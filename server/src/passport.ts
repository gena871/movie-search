import { getRepository } from 'typeorm';
import { User } from './entity/User';
import passport from 'passport';
import passportGoogle from 'passport-google-oauth20';

const GoogleStrategy = passportGoogle.Strategy;

passport.serializeUser(async (user, done) => {
    done(null, user);
});

passport.deserializeUser(async (user: User, done) => {
    done(null, user);
});

passport.use(
    new GoogleStrategy(
        {
            clientID: '50809916833-7a2cj4vjn0s5a073r8so13jjhrumofmp.apps.googleusercontent.com',
            clientSecret: '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', //process.env.GOOGLE_CLIENT_SECRET ,
            callbackURL: 'http://localhost:3000/google/redirect',
            passReqToCallback: true,
        },
        async (req, accessToken, refreshToken, profile, done) => {
            const user = await getRepository(User).findOne({ googleId: profile.id });
            // console.log(user)
            // If user doesn't exist creates a new user. (similar to sign up)
            if (!user) {
                //  console.log("created")
                const newUser = new User();
                newUser.googleId = profile.id;
                newUser.login = profile.displayName;
                newUser.email = profile.emails?.[0].value || '';
                await getRepository(User).save(newUser);
                if (newUser) {
                    done(null, newUser, accessToken);
                }
            } else {
                //   console.log("loh")
                done(null, user, accessToken);
            }
        },
    ),
);

export default passport;
