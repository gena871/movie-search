import { Router } from 'express';
import FilterMoviesRouter from '../routes/filter-movies';
import FavoriteRouter from '../routes/favorite';
import AuthRouter from '../routes/auth';

const router = Router();

router.use(FilterMoviesRouter);
router.use(FavoriteRouter);
router.use(AuthRouter);

export default router;
