import { Router } from 'express';
import FilterMovieController from '../controllers/filter-movie-controller';
import { checkAuth } from '../middleware/checkAuth';

const router = Router();
const filterMovieController = new FilterMovieController();

router.get('/heathCheck', checkAuth, filterMovieController.heathCheck);
router.get('/filterMovies', filterMovieController.filterMovies);
router.get('/getById', filterMovieController.getById);

export default router;
