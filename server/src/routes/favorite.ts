import { Request, Response, Router } from 'express';
import FavoriteController from '../controllers/favorite-controller';
import { checkAuth } from '../middleware/checkAuth';

const router = Router();
const favoriteController = new FavoriteController();

router.post('/favorite/add', checkAuth, favoriteController.addToFavorite);
router.delete('/favorite/delete', checkAuth, favoriteController.deleteToFavorite);
router.get('/favorite/get', checkAuth, favoriteController.getFavorite);
router.get('/favorite/check', checkAuth, favoriteController.checkMovieIsFavourite);

export default router;
