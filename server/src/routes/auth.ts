import { Router } from 'express';
import AuthController from '../controllers/auth-controller';
import passport from 'passport';
const router = Router();
const authController = new AuthController();
import '../entity/User';
import { checkAuth } from '../middleware/checkAuth';

router.get('/login', checkAuth, authController.login);

router.get('/failed', (req, res) => {
    res.send('Failed');
});

router.get('/success', checkAuth, authController.success);

router.get(
    '/google',
    passport.authenticate('google', {
        scope: ['email', 'profile'],
    }),
);

router.get(
    '/google/redirect',
    passport.authenticate('google', {
        failureRedirect: '/failed',
    }),
    authController.googleRedirect,
);

router.get('/logout', checkAuth, authController.logout);

export default router;
