import { Column, MigrationInterface, PrimaryGeneratedColumn, QueryRunner } from 'typeorm';

export class UserTable1631274974389 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`create table "user" (id uuid default uuid_generate_v4() not null constraint user_pk primary key, login      character varying(40), email character varying(40), "googleId" character varying(40), created_at TIMESTAMP default now() not null, updated_at TIMESTAMP default now() not null);
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "user"`);
    }
}
