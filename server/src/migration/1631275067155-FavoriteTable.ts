import { MigrationInterface, QueryRunner } from 'typeorm';

export class FavoriteTable1631275067155 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`create table favourite_user
            (id serial not null constraint favourite_user_pk primary key, user_id uuid not null constraint favourite_user_user_fk references "user" (id) on update cascade on delete cascade, movie_id   int                     not null, created_at TIMESTAMP default now() not null, updated_at TIMESTAMP default now()
            );

        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "favourite_user"`);
    }
}
