import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import morgan from 'morgan';
import BaseRouter from './routes/index';

const cookieSession = require('cookie-session');
import passport from 'passport';
import './passport';

const app = express();
app.use(bodyParser.json());
app.use(
    cors({
        origin: [`${process.env.CLIENT_BASE_URL}`, `http://localhost:3001`],
        credentials: true,
        allowedHeaders: 'Content-Type, Authorization',
    }),
);
app.use(morgan('dev'));
app.use(
    cookieSession({
        name: 'google-auth-session',
        maxAge: 1000 * 60 * 60,
        keys: ['key1', 'key2'],
    }),
);
app.use(passport.initialize());
app.use(passport.session());
app.use('/', BaseRouter);
export default app;
