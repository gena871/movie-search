declare global {
    namespace Express {
        interface User extends User {
            id: string;
            email: string;
        }
    }
}
