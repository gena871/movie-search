# Поск фильмов
____
## Описание
Приложение позволяет искать фильмы и добавлять их в избранное. Используется Google OAuth 2.0 авторизация

## Инструментарий
1. Фронт: **React, Material UI, TypeScript**
2. Сервер **Node.js, Express.js**
3. База данных **Postgres**
4. [Неофициальное API Кинопоиска](https://kinopoisk.dev/) 

## Интерфейс
### Верхняя панель с авторизацией в Google
![nav](https://live.staticflickr.com/65535/51773329675_47f9f04ce0_b.jpg)

### Главная страница
![nav](https://live.staticflickr.com/65535/51772443966_fab04c9086_b.jpg)
![nav](https://live.staticflickr.com/65535/51771622667_25c858a81e_z.jpg)
![nav](https://live.staticflickr.com/65535/51772685403_e9861d9ffa_b.jpg)

### Страница фильма
![nav](https://live.staticflickr.com/65535/51773329550_4f99281280_b.jpg)
![nav](https://live.staticflickr.com/65535/51771622522_d6b7fcc1c9_b.jpg)
![nav](https://live.staticflickr.com/65535/51773329515_24f956ae83_z.jpg)
![nav](https://live.staticflickr.com/65535/51772685428_bc9f79a86a_z.jpg)
![nav](https://live.staticflickr.com/65535/51773114094_e42f8e701a_b.jpg)
![nav](https://live.staticflickr.com/65535/51773329450_f4f8ca16c9_z.jpg)

### Избранное
![nav](https://live.staticflickr.com/65535/51771622572_4c3b2547ec_b.jpg)
![nav](https://live.staticflickr.com/65535/51773092149_efd306da0c_z.jpg)
![nav](https://live.staticflickr.com/65535/51773360330_5222f6b82a_b.jpg)
![nav](https://live.staticflickr.com/65535/51772474966_b045e4024c_z.jpg)